# README #

### What is this repository for? ###
+ This git is created for only purpose that strengthen own math ability.
+ It compose 4 course:
	+ Linear algebra: https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/
	+ Multivariable calculus: https://ocw.mit.edu/courses/mathematics/18-02sc-multivariable-calculus-fall-2010/
	+ Differential equations: https://ocw.mit.edu/courses/mathematics/18-03sc-differential-equations-fall-2011/
	+ Probabilistic systems: https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-041sc-probabilistic-systems-analysis-and-applied-probability-fall-2013/

### How do I get set up? ###
+ Nothing 

### Guidelines ###
+ We will start with linear algebra and multivariable calculus.
+ Offline meeting: every saturday. 8h -> 10h30 am (HCM University of Technology)

### Who do I talk to? ###
+ I will create a group chat on facebook or skype for discussion purposes